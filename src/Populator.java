import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckbox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDXFA;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * Created by maccesch on 20.11.15.
 * Taken from http://www.daedtech.com/programatically-filling-out-pdfs-in-java
 */
public class Populator {

    private static PDDocument _pdfDocument;

    public static void populateAndCopy(File originalPdf, String targetPdf, List<Map<String, Object>> valuesForMonth)
            throws IOException, COSVisitorException {
        _pdfDocument = PDDocument.load(originalPdf);

//        printFields();  //Uncomment to see the fields in this document in console

//        for (Map.Entry<String, String> pair : fieldValues.entrySet()) {
//            setField(pair.getKey(), pair.getValue());
//        }

        int i = 0;
        for (Map<String, Object> fieldValues : valuesForMonth) {

            LocalDate startDate = ((Date) fieldValues.get("startDate")).toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();

            int fieldIdx = i * 9;

            setField(String.format("Text%d.Text%d", fieldIdx + 1, fieldIdx + 1),
                     String.format("%02d", startDate.getDayOfMonth()));
            setField(String.format("Text%d.Text%d", fieldIdx + 2, fieldIdx + 2),
                     String.format("%02d", startDate.getMonthValue()));
            setField(String.format("Text%d.Text%d", fieldIdx + 3, fieldIdx + 3),
                     String.format("%04d", startDate.getYear()));

            Object congregationNo = fieldValues.get("congregationNo");
            if (congregationNo != null) {
                setField(String.format("Text%d.Text%d", fieldIdx + 5, fieldIdx + 5), congregationNo.toString());
            }

            Object congregationName = fieldValues.get("congregationName");
            if (congregationName != null) {
                setField(String.format("Text%d.Text%d", fieldIdx + 4, fieldIdx + 4),
                         congregationName.toString());
            }

            ++i;
        }
        _pdfDocument.save(targetPdf);
        _pdfDocument.close();
    }

    public static void setField(String name, String value) throws IOException {
        PDDocumentCatalog docCatalog = _pdfDocument.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();
        acroForm.getDictionary().setItem(COSName.getPDFName("NeedAppearances"), COSBoolean.TRUE);
        PDField field = acroForm.getField(name);
        if (field != null) {

            if (Objects.equals(value, "<checked>")) {
                ((PDCheckbox) field).check();
                field.setValue(((PDCheckbox) field).getOnValue());
            } else if (Objects.equals(value, "<unchecked>")) {
                ((PDCheckbox) field).unCheck();
                field.setValue(((PDCheckbox) field).getOffValue());
            } else {
                field.setValue(value);
            }
        } else {
            System.err.println("No field found with name:" + name);
        }
    }

    @SuppressWarnings("rawtypes")
    public static void printFields() throws IOException {
        PDDocumentCatalog docCatalog = _pdfDocument.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();
        List fields = acroForm.getFields();
        Iterator fieldsIter = fields.iterator();

        System.out.println(Integer.toString(fields.size()) + " top-level fields were found on the form");

        while (fieldsIter.hasNext()) {
            PDField field = (PDField) fieldsIter.next();
            processField(field, "|--", field.getPartialName());
        }
    }

    @SuppressWarnings("rawtypes")
    private static void processField(PDField field, String sLevel, String sParent) throws IOException {
        List kids = field.getKids();
        if (kids != null) {
            Iterator kidsIter = kids.iterator();
            if (!sParent.equals(field.getPartialName())) {
                sParent = sParent + "." + field.getPartialName();
            }

            System.out.println(sLevel + sParent);

            while (kidsIter.hasNext()) {
                Object pdfObj = kidsIter.next();
                if (pdfObj instanceof PDField) {
                    PDField kid = (PDField) pdfObj;
                    processField(kid, "|  " + sLevel, sParent);
                }
            }
        } else {
            String outputString =
                    sLevel + sParent + "." + field.getPartialName() + ",  type=" + field.getClass().getName();
            System.out.println(outputString);
        }
    }
}

import com.healthmarketscience.jackcess.*;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.jdatepicker.JDatePanel;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;


public class FileIoForm {
    private JButton openPDFButton;
    private JButton openAccessDBButton;
    private JButton selectOutputPathButton;
    private JButton generateButton;
    private JPanel rootPanel;
    private JTextField pdfTextField;
    private JTextField accessTextField;
    private JTextField outputTextField;
    private JDatePanel datePanel;

    private File pdfFile;
    private File accessFile;
    private File outputPath;

    private LocalDate lastGeneratedDate = null;

    private final String inputFilename = "input.prop";

    public static void main(String[] args) {
        JFrame frame = new JFrame("FileIoForm");
        frame.setContentPane(new FileIoForm().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setTitle("Fill PDF Forms");
    }

    private void saveInput() {
        File file = new File(inputFilename);
        Properties p = new Properties();

        if (pdfFile != null) {
            try {
                p.setProperty("pdfFile", pdfFile.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (accessFile != null) {
            try {
                p.setProperty("accessFile", accessFile.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (outputPath != null) {
            try {
                p.setProperty("outputPath", outputPath.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (lastGeneratedDate != null) {
            p.setProperty("startDate", lastGeneratedDate.toString());
        }

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            p.store(bw, "Input of user");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadInput() {
        File file = new File(inputFilename);
        Properties p = new Properties();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            p.load(br);

            String pdfFilename = p.getProperty("pdfFile");
            if (pdfFilename != null) {
                this.pdfFile = new File(pdfFilename);
                if (this.pdfFile != null) {
                    pdfTextField.setText(this.pdfFile.getName());
                }
            }

            String accessFilename = p.getProperty("accessFile");
            if (accessFilename != null) {
                this.accessFile = new File(accessFilename);
                if (this.accessFile != null) {
                    accessTextField.setText(this.accessFile.getName());
                }
            }

            String outputPathname = p.getProperty("outputPath");
            if (outputPathname != null) {
                this.outputPath = new File(outputPathname);
                if (this.outputPath != null) {
                    outputTextField.setText(this.outputPath.getName());
                }
            }

            String startDate = p.getProperty("startDate");
            if (startDate != null) {
                LocalDate date = LocalDate.parse(startDate);
                datePanel.getModel().setDate(date.getYear(), date.getMonthValue() - 1, date.getDayOfMonth());
                datePanel.getModel().setSelected(true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public FileIoForm() {
        loadInput();

        openPDFButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser fc = new JFileChooser();
                fc.setFileFilter(new FileNameExtensionFilter("PDF Files", "pdf"));

                fc.setSelectedFile(pdfFile);

                int ret = fc.showOpenDialog(rootPanel);

                if (ret == JFileChooser.APPROVE_OPTION) {
                    pdfFile = fc.getSelectedFile();
                    pdfTextField.setText(pdfFile.getName());

                    saveInput();
                }
            }
        });
        openAccessDBButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser fc = new JFileChooser();
                fc.setFileFilter(new FileNameExtensionFilter("Access Files", "mdb accdb"));

                fc.setSelectedFile(accessFile);

                int ret = fc.showOpenDialog(rootPanel);

                if (ret == JFileChooser.APPROVE_OPTION) {
                    accessFile = fc.getSelectedFile();
                    accessTextField.setText(accessFile.getName());

                    saveInput();
                }
            }
        });
        selectOutputPathButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser fc = new JFileChooser();
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                fc.setSelectedFile(outputPath);

                int ret = fc.showOpenDialog(rootPanel);

                if (ret == JFileChooser.APPROVE_OPTION) {
                    outputPath = fc.getSelectedFile();
                    outputTextField.setText(outputPath.getName());

                    saveInput();
                }
            }
        });
        generateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GregorianCalendar startDate = (GregorianCalendar) datePanel.getModel().getValue();

                try {

                    List<Map<String, Object>> fieldValuesList = getFieldValuesFromDatabase(startDate.getTime());

                    if (fieldValuesList == null) {
                        // TODO : error msg
                        return;
                    }

                    List<Map<String, Object>> filteredFieldValues = new LinkedList<>();
                    for (Map<String, Object> fieldValues : fieldValuesList) {
                        if (fieldValues.get("startDate") != null) {
                            filteredFieldValues.add(fieldValues);
                        }
                    }

                    filteredFieldValues
                            .sort((o1, o2) -> ((Date) o1.get("startDate")).toInstant().atZone(ZoneId.systemDefault())
                                    .toLocalDate()
                                    .isBefore(((Date) o2.get("startDate")).toInstant().atZone(ZoneId.systemDefault())
                                                      .toLocalDate()) ? -1 : 1);

                    List<Map<String, Object>> valuesForMonth = new ArrayList<Map<String, Object>>();
                    int prevMonth = -1;
                    int prevYear = -1;

                    for (Map<String, Object> fieldValues : filteredFieldValues) {

                        LocalDate date = ((Date) fieldValues.get("startDate")).toInstant()
                                .atZone(ZoneId.systemDefault())
                                .toLocalDate();
                        int month = date.getMonthValue();

                        if (month != prevMonth && valuesForMonth.size() > 0) {
                            Populator.populateAndCopy(pdfFile,
                                                      outputPath.getCanonicalPath() + File.separator +
                                                      pdfFile.getName().split("\\.")[0] + "_" +
                                                      prevYear + "-" + prevMonth +
                                                      ".pdf", valuesForMonth);
                            valuesForMonth.clear();
                        }

                        valuesForMonth.add(fieldValues);
                        prevMonth = month;
                        prevYear = date.getYear();

//                        fieldValues.put("Favourite Colour List Box.Favourite Colour List Box", "Orange");
//                        fieldValues.put("Language 1 Check Box.Language 1 Check Box", "<unchecked>");
//                        fieldValues.put("Language 2 Check Box.Language 2 Check Box", "<unchecked>");
//                        fieldValues.put("Language 3 Check Box.Language 3 Check Box", "<unchecked>");
//                        fieldValues.put("Language 4 Check Box.Language 4 Check Box", "<unchecked>");
//                        fieldValues.put("Language 5 Check Box.Language 5 Check Box", "<unchecked>");
//                        fieldValues.put("Driving License Check Box.Driving License Check Box", "<checked>");
                    }

                    Populator.populateAndCopy(pdfFile,
                                              outputPath.getCanonicalPath() + File.separator +
                                              pdfFile.getName().split("\\.")[0] + "_" +
                                              prevYear + "-" + prevMonth +
                                              ".pdf", valuesForMonth);

//                    lastGeneratedDate = LocalDate.now();
                    saveInput();

                } catch (IOException | COSVisitorException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    private List<Map<String, Object>> getFieldValuesFromDatabase(Date afterDate) {

        try {
            Database db = DatabaseBuilder.open(accessFile);
//            System.out.print("Tables: ");
//            System.out.println(db.getTableNames());

            Table visitTable = db.getTable("tblBesWoDat");
            Table congregationTable = db.getTable("tblVS");
//            System.out.print("Columns: ");
//            System.out.println(visitTable.getColumns());

            List<Map<String, Object>> fieldValuesList = new LinkedList<>();

            for (Row row : visitTable) {
                Map<String, Object> fieldValues = new HashMap<>();

                Date startDate = (Date) row.get("tBAnfDat");

                if (startDate.compareTo(afterDate) > -1) {

                    Integer congregationId = (Integer) row.get("tBVId");

                    Row congregationRow = CursorBuilder
                            .findRow(congregationTable, Collections.singletonMap("tVsId", congregationId));

                    if (congregationRow != null) {
                        String congregationName = (String) congregationRow.get("tVsName");
                        String congregationNo = (String) congregationRow.get("tVsNr");

                        if (congregationName.length() < 2) {
                            congregationName = (String) row.get("tBWoZus");
                        }

                        fieldValues.put("startDate", startDate);
                        fieldValues.put("congregationName", congregationName);
                        fieldValues.put("congregationNo", congregationNo);
                    }

                    fieldValuesList.add(fieldValues);
                }
            }

            return fieldValuesList;

        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return null;
    }
}
